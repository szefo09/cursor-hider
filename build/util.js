'use strict';

const fs = require('fs');
const path = require('path');

function getBaseDir() {
	return path.join(__dirname, '..');
}

function readPackageJson() {
	return JSON.parse(fs.readFileSync(path.join(getBaseDir(), 'package.json')));
}


module.exports = {
	getBaseDir,
	readPackageJson,
};
